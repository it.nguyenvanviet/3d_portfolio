import {
    mobile,
    backend,
    creator,
    web,
    javascript, 
    aspnetcore,
    reactjs, 
    tailwind,
    nodejs, 
    sqlserver,
    git, 
    docker,
    meta, 
    shopify, 
    openAI_DALL_E, 
    dashboard,
    threejs,
    nftgamecard
  } from "../assets";
  
  export const navLinks = [
    {
      id: "about",
      title: "About",
    },
    {
      id: "work",
      title: "Work",
    },
    {
      id: "contact",
      title: "Contact",
    },
  ];
  
  const services = [
    {
      title: "Web Developer",
      icon: web,
    },
    {
      title: "ReactJS Developer",
      icon: mobile,
    },
    {
      title: "Frontend Developer",
      icon: creator,
    },
    {
      title: "Backend Developer",
      icon: backend,
    }, 
  ];
  
  const technologies = [
    {
      name: "JavaScript",
      icon: javascript,
    },
    {
      name: "React JS",
      icon: reactjs,
    },
    {
      name: "C#",
      icon: aspnetcore,
    },
    {
      name: "Tailwind CSS",
      icon: tailwind,
    },
    {
      name: "Node JS",
      icon: nodejs,
    },
    {
      name: "SQLServer",
      icon: sqlserver,
    },
    {
      name: "git",
      icon: git,
    },
    {
      name: "docker",
      icon: docker,
    },
  ];
  
  const experiences = [
    {
      title: "Freelancer",
      company_name: "Chaovietnam.vn",
      icon: shopify,
      iconBg: "#383E56",
      date: "Nov 2014 - Dec 2015",
      points: [
        "-  Website system administration",
        "-  Building functionals on 3 main websites chaovietnam.vn, laisuat.vn, bacsigiadinh.com",
      ],
    },
    {
      title: "Angular Developer - Freelancer",
      company_name: "Ytegiadinh.vn",
      icon: shopify,
      iconBg: "#E6DEDD",
      date: "Jan 2021 - Feb 2022",
      points: [
        "-  Building and supporting the operation of the backend system build by AngularJs and asp.net MVC",
        "-  Designing and coding E - commerce website.",
      ],
    },
    {
      title: "Web Developer",
      company_name: "Fpt Telecom",
      icon: shopify,
      iconBg: "#383E56",
      date: "Jan 2018 - June 2022",
      points: [
        "-  Build and implement functional programs about Camera and FPT television.",
        "-  Testing and deploying programs and systems, fixing and improving existing software.",
        "-  Support to connect with related teams. Gather and evaluate user feedback.",
        "-  Create technical documentation for reference and reporting."
      ],
    },
    {
      title: "Full stack Developer",
      company_name: "FPT Software",
      icon: meta,
      iconBg: "#E6DEDD",
      date: "July 2022 - Present",
      points: [
        "Developing and maintaining web applications using Node.js and other related technologies.",
        "Collaborating with cross-functional teams including leader, product managers, and other developers to create high-quality products.",
        "Implementing responsive design and ensuring cross-browser compatibility.",
        "Participating in code reviews and providing constructive feedback to other developers.",
      ],
    },
  ];
  
  const testimonials = [
    {
      testimonial:
        "I thought it was impossible to make a website as beautiful as our product, but Rick proved me wrong.",
      name: "Sara Lee",
      designation: "CFO",
      company: "Acme Co",
      image: "https://randomuser.me/api/portraits/women/4.jpg",
    },
    {
      testimonial:
        "I've never met a web developer who truly cares about their clients' success like Rick does.",
      name: "Chris Brown",
      designation: "COO",
      company: "DEF Corp",
      image: "https://randomuser.me/api/portraits/men/5.jpg",
    },
    {
      testimonial:
        "After Rick optimized our website, our traffic increased by 50%. We can't thank them enough!",
      name: "Lisa Wang",
      designation: "CTO",
      company: "456 Enterprises",
      image: "https://randomuser.me/api/portraits/women/6.jpg",
    },
  ];
  
  const projects = [
    {
      name: "Dashboard",
      description:
        "Template admin for shopify",
      tags: [
        {
          name: "react",
          color: "blue-text-gradient",
        },
        {
          name: "mongodb",
          color: "green-text-gradient",
        },
        {
          name: "tailwind",
          color: "pink-text-gradient",
        },
      ],
      image: dashboard,
      source_code_link: "https://gitlab.com/it.nguyenvanviet/template-ecommerce",
    },
    {
      name: "Open AI dall-e",
      description:
        "Web application that enables create and shared images with Dall-E AI",
      tags: [
        {
          name: "react",
          color: "blue-text-gradient",
        },
        {
          name: "restapi",
          color: "green-text-gradient",
        },
        {
          name: "tailwind",
          color: "pink-text-gradient",
        },
        {
          name: "openai",
          color: "white-text-gradient",
        },
      ],
      image: openAI_DALL_E,
      source_code_link: "https://gitlab.com/it.nguyenvanviet/dall-e",
    },
    {
      name: "NFT card game",
      description:
        "NFT card game use token to play game",
      tags: [
        {
          name: "react",
          color: "blue-text-gradient",
        },
        {
          name: "solidity",
          color: "green-text-gradient",
        },
        {
          name: "tailwind",
          color: "pink-text-gradient",
        }, 
      ],
      image: nftgamecard,
      source_code_link: "https://gitlab.com/it.nguyenvanviet/nft-cardgame",
    },
  ];
  
  export { services, technologies, experiences, testimonials, projects };